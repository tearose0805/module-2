import { Modal } from "./services/modal/modalService";
//функция создает первое модальное окно и соответствующий объект JS
//при нажатии кнопки Открыть окно 1
function createModal1(id = null) {
    document.querySelector('.but-open-win1').addEventListener('click', e => {
        e.preventDefault();
        const template = "<div>Модальное окно 1</div>";
        const modal = new Modal(id);
        modal.open(template, 'first');
    });
}
//функция создает второе модальное окно и соответствующий объект JS
//при нажатии кнопки Открыть окно 2
function createModal2(id = null) {
    document.querySelector('.but-open-win2').addEventListener('click', e => {
        e.preventDefault();
        const template = "<div>Модальное окно 2</div>";
        const modal = new Modal(id);
        modal.open(template, 'second');
    });
}
//функция удалет окно и объект JS по идентификатору, если он передан
//или удаляет все окна последовательно
//при нажатии кнопки Удалить окно
function removeOneModal(id = null) {
    document.querySelector('.but-del').addEventListener('click', e => {
        e.preventDefault();
        //если id задан или 0
        if (id !== null) {
            const ind = Modal.removeById(id);
            //console.log("Ind not -1: "+ind);
            //обработка при отсутствии объекта с заданным идентификатором в массиве
            if (ind == -1) {
                console.log(`Нет модального окна с id: ${id}`);
                //console.log("Ind -1: "+ind);
                return;
            }
            //если id не задан, т.е. null
        }
        else {
            Modal.removeOneByOne();
        }
    });
}
//функция удалет все окна со стр. и все объекты JS
//по нажатии кнопки Удалить все
function removeAllModals() {
    document.querySelector('.but-del-all').addEventListener('click', e => {
        e.preventDefault();
        Modal.removeAll();
    });
}
createModal1('1');
createModal2('2');
removeOneModal('1');
removeAllModals();

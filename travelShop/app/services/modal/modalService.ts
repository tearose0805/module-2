export class Modal {
    private readonly id: string;
    public static modalList: any[] = [];

    constructor(id: string = null) {
        Modal.removeById(id);
        Modal.modalList.push(this);
        console.log("массив модальных окон из класса: ", Modal.modalList); //для контроля
        this.id = id;

    }

    public open(template: string, att:string):void {
        const divWrap = document.createElement("div");
        divWrap.innerHTML = template;
        divWrap.id = this.id;
        divWrap.setAttribute('modal-id', att);
        divWrap.classList.add('modal-element');
        document.querySelector('.contain-modal').append(divWrap);
    }

    public removeModalEl():void {
        const modalEl = document.getElementById(this.id);
        if(modalEl){
            modalEl.remove();
        }
    }

    public static removeById(id: string):number {
        const ind = Modal.modalList.findIndex(item => item.id === id);
        if (ind !== -1) { //код выполняем, если найден индекс
            const FindObj = Modal.modalList[ind];
            FindObj.removeModalEl();
            Modal.modalList.splice(ind, 1);
            console.log("Массив после изменения", Modal.modalList); //для контроля
        }
        //возвращаем результат работы метода findIndex для дальнейшей обработки в функции removeOneModal
        return ind;
    }

    public static removeOneByOne():void{
        console.log('Массив перед удалением:', Modal.modalList); //для контроля
        if(Modal.modalList.length > 0){
            const lastEl = Modal.modalList.pop();
            lastEl.removeModalEl();
        }
    }

    public static removeAll(): void{
        console.log('Удаление всех окон');
        for (let i = Modal.modalList.length-1; i>=0; i--){
            Modal.removeOneByOne();
        }
    }
}

//функция создает первое модальное окно и соответствующий объект JS
//при нажатии кнопки Открыть окно 1
function createModal1(id: string=null){
    document.querySelector('.but-open-win1').addEventListener('click',
        e =>{
            e.preventDefault();
            const template = "<div>Модальное окно 1</div>";
            const modal = new Modal(id);
            modal.open(template,'first');
        });
}


//функция создает второе модальное окно и соответствующий объект JS
//при нажатии кнопки Открыть окно 2
function createModal2(id:string = null){
    document.querySelector('.but-open-win2').addEventListener('click',
        e =>{
            e.preventDefault();
            const template = "<div>Модальное окно 2</div>";
            const modal = new Modal(id);
            modal.open(template,'second');
        });
}

//функция удалет окно и объект JS по идентификатору, если он передан
//или удаляет все окна последовательно
//при нажатии кнопки Удалить окно

function removeOneModal(id:string = null){
    document.querySelector('.but-del').addEventListener('click',
        e => {
            e.preventDefault();

            //если id задан или 0
            if(id!==null){
                const ind = Modal.removeById(id);
                //console.log("Ind not -1: "+ind);
               //обработка при отсутствии объекта с заданным идентификатором в массиве
               if(ind == -1){
                    console.log(`Нет модального окна с id: ${id}`);
                    //console.log("Ind -1: "+ind);
                   return;
                 }
               //если id не задан, т.е. null
            } else{
                Modal.removeOneByOne();
            }
        });
}

//функция удалет все окна со стр. и все объекты JS
//по нажатии кнопки Удалить все
function removeAllModals(){
    document.querySelector('.but-del-all').addEventListener('click',
        e => {
            e.preventDefault();
            Modal.removeAll();
        });
}


createModal1('1');
createModal2('2');
removeOneModal('1');
removeAllModals();


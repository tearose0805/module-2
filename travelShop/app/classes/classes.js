class User {
    constructor(name, location) {
        this.name = name;
        this.location = location;
        this._uid = '123-22-451-23';
    }
    getUserName() {
        return this.name;
    }
    setUserName(name) {
        this.name = name;
        this.privateMethod();
    }
    get uid() {
        return this._uid;
    }
    set uid(uid) {
        this._uid = uid;
    }
    set docType(type) {
        this._docType = type;
    }
    get docType() {
        return this._docType;
    }
    privateMethod() {
        console.log(this.name);
    }
}
class UserVip extends User {
    constructor(name, location, cardNumber) {
        super(name, location);
        this.cardNumber = cardNumber;
    }
    setCardNumber(cardNumber) {
        this.cardNumber = cardNumber;
    }
    getCardNumber() {
        return this.cardNumber;
    }
}
export {};

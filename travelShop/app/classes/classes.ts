import {IUser,IDocType,IUserVip} from "module-2/travelshop/app/models/models"
class User implements IUser,IDocType{
    private _uid: string = '123-22-451-23'
    private _docType: string;

    constructor(protected name: string, protected location: string){
    }
    public getUserName(): string{
        return this.name;
    }
    public setUserName (name: string): void{
        this.name = name;
        this.privateMethod();

    }
    public get uid(): string{
       return this._uid;
    }
    public set uid(uid: string){
        this._uid = uid;
    }

    public set docType (type: string){
        this._docType = type;
    }

    public get docType(){
        return this._docType;
    }

    private privateMethod(): void{
        console.log(this.name);
    }
}

class UserVip extends User implements IUserVip{
    constructor(name: string, location: string, private cardNumber:number) {
        super(name, location);
    }

    public setCardNumber(cardNumber: number){
        this.cardNumber = cardNumber;
    }

    public getCardNumber(): number{
        return this.cardNumber;
    }
}
